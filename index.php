<?php
session_start();

if ($_SERVER["HTTP_HOST"] == 'localhost') {
    $base_url = "http://localhost/empireland";
    //$base_api = "http://localhost/empireapi/api";
    $base_api = "http://45.118.132.77/api";
} else {
    // $base_url = "http://register.empirefit.club";
    // $base_api = "http://45.118.132.77/api";

    $base_url = "https://register.empirefit.club";
    $base_api = "https://api.empirefit.club/api";
}
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Empire Fit | Online Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.default.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <style>
        label {
            font-size: 12px;
        }

        .form-control {
            font-size: 16px;
        }

        .english {
            font-size: 14px !important;
        }

        .indonesian {
            font-size: 16px !important;
        }
    </style>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

    <header class="py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-2">
                    <img src="images/logo.png" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 col-10 mb-3 mb-md-0">
                    <div class="addresses"></div>
                </div>
                <div class="col-md-7 col-12 text-md-right mt-3 mt-md-0">
                    <h3>Online Registration &amp; Agreement</h3>
                    <div class="row justify-content-end mt-3 select-branch">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Branch</label>
                                </div>
                                <select name="branch" id="branch" class="branch-pick custom-select">
                                    <option value="">Select Branch</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <hr>

    <section class="pt-4 pb-5 before-registration">
        <div class="container">
            <div class="row mb-4">
                <div class="col-12">
                    <div class="alert alert-secondary">
                        <div>
                            <strong class="indonesian">ASUMSI RISIKO; PENGECUALIAN; dan TANGGUNG JAWAB ("Perjanjian")</strong>
                        </div>
                        INI ADALAH PERSETUJUAN PELEPASAN TANGGUNG JAWAB DENGAN MENANDATANGANI PERJANJIAN, DIMANA ANDA MELEPASKAN HAK HUKUM TERTENTU DENGAN SEGALA KELUHAN YANG TERJADI. HARAP DIBACA DENGAN SAKSAMA.
                        <br><br>

                        <div>
                            <strong class="english"><i>ASSUMPTION OF RISK; WAIVER; and LIABILITY RELEASE (the "Agreement'')</i></strong>
                        </div>
                        <i class="english">THIS IS A LIABILITY RELEASE BY AFFIRMATIVELY AGREEING TO IT BY SIGNING THE AGREEMENT, YOU ARE WAIVING CERTAIN LEGAL RIGHTS AND ARE COMPLETELY RELEASING POTENTIAL CLAIMS. PLEASE READ IT CAREFULLY.</i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 mb-4">
                                    <h6 class="mb-2 fancy-head"><span>I. Data Pribadi / <i class="english">Personal Data of Participants</i></span></h6>
                                </div>
                                <div class="form-group col-md-6 mb-4">
                                    <label>Nama Depan / <i class="english">Fisrt Name</i></label>
                                    <input type="text" class="form-control" name="firstName" placeholder="Name On ID Card">
                                </div>
                                <div class="form-group col-md-6 mb-4">
                                    <label>Nama Belakang / <i class="english">Last Name</i></label>
                                    <input type="text" class="form-control" name="lastName" placeholder="Name On ID Card">
                                </div>
                                <div class="form-group col-md-6 mb-4">
                                    <label>Nama Panggilan / <i class="english">Nickname</i></label>
                                    <input type="text" class="form-control" name="nickName" placeholder="Nick Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Tanggal Lahir / <i class="english">Date of Birth</i></label>
                                    <input required type="date" class="form-control" name="datebirth" aria-describedby="emailHelp" placeholder="Date of Birth">
                                </div>
                                <!-- <div class="form-group col-md-6">
                                    <label for="phone">ID Card Number(KTP/SIM/Passport)</label>
                                    <input required type="text" class="form-control" id="idcard" name="idcard" placeholder="Insert Your ID Card Number">
                                </div> -->
                                <div class="form-group col-md-6 mb-4">
                                    <label for="lastName">Nomor Kontak / <i class="english">Contact Number</i></label>
                                    <input required type="text" class="form-control" id="contactnumber" name="contactnumber" placeholder="Contact Number">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Alamat Email / <i class="english">Email address</i></label>
                                    <input required type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Email Address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Pekerjaan / <i class="english">Occupation</i></label>
                                    <input type="email" class="form-control" name="occupation" aria-describedby="occupation" placeholder="Occupation">
                                </div>
                                <!-- <div class="form-group col-md-6 mb-4 agreement">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" placeholder="Address" >
                                </div>
                                <div class="form-group col-md-6 mb-4 agreement">
                                    <label>In an emergency, I would like EMPIRE FIT CLUB to Call:</label>
                                    <input type="text" class="form-control" name="emergency" placeholder="Emergency Number" >
                                </div> -->
                            </div>

                            <div class="row mt-4 agreement">
                                <div class="col-12 mb-4">
                                    <h6 class="mb-0 fancy-head"><span>II. Alamat Peserta / <i class="english">Participant Address</i></span></h6>
                                </div>

                                <div class="form-group col-md-6 mb-4">
                                    <label>Alamat Tinggal / <i class="english">Home Address</i></label>
                                    <textarea class="form-control" name="address" id="" cols="30" rows="4"></textarea>
                                </div>
                                <div class="form-group col-md-6 mb-4">
                                    <label>Wilayah / <i class="english">Home Address Area</i></label>
                                    <select class="area prettyselect" name="home_address_area">
                                    </select>
                                </div>
                                <div class="form-group col-md-6 mb-4">
                                    <label>Tempat Kerja / <i class="english">Work Address</i></label>
                                    <textarea class="form-control" name="work_address" id="" cols="30" rows="4"></textarea>
                                </div>
                                <div class="form-group col-md-6 mb-4">
                                    <label>Wilayah Tempat Kerja / <i class="english">Work Address Area</i></label>
                                    <select class="area prettyselect" name="work_address_area">
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-4 agreement">
                                <div class="col-12 mb-4">
                                    <h6 class="mb-0 fancy-head"><span>III. Kontak Darurat / <i class="english">Participant Emergency Contact</i></span></h6>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Emergency Contact Name</label>
                                    <input type="email" class="form-control" name="emergency_contact_name" aria-describedby="emergency_contact_name" placeholder="emergency contact name">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Nama Kontak Darurat / <i class="english">Emergency Contact Name</i></label>
                                    <input type="email" class="form-control" name="emergency_contact_number" aria-describedby="emergency_contact_number" placeholder="emergency contact number">
                                </div>
                            </div>

                            <div class="row mt-4 agreement">
                                <div class="col-12 mb-0">
                                    <h6 class="mb-0 fancy-head"><span>IV. Riwayat Kesehatan / <i class="english">Health Questions</i></span></h6>
                                </div>

                                <div class="col-12">
                                    <div class="questions"></div>
                                </div>
                            </div>

                            <div class="row mt-4 agreement">
                                <div class="col-12 mb-3">
                                    <h6 class="mb-2 fancy-head"><span>V. Catatan / <i class="english">Notes</i></span></h6>
                                </div>

                                <div class="col-12">
                                    <textarea class="form-control" name="reviewer_note" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="row mt-4 agreement">
                                <div class="col-12 mb-3">
                                    <h6 class="mb-2 fancy-head"><span>VI. Fotografi dan Vidio Rilis / <i class="english">Photography and Video Release</i></span></h6>
                                </div>

                                <div class="col-12 text-justify">
                                    <p class="indonesian">Peserta yang terlibat dalam seluruh kegiatan BILGYM dapat difoto atau direkam dalam video selama pelatihan. Yang bertanda tangan di bawah ini dengan ini menyetujui penggunaan foto-foto dan / atau video ini tanpa kompensasi, di situs web BILGYM atau dalam materi editorial, promosi atau pemasaran yang diproduksi dan / atau diterbitkan oleh BILGYM.</p>
                                    <p class="english"><i>Photography and/or Video Release of Participant involved in any activities offered by BILGYM may be photographed or videotaped during training. The undersigned hereby consents to the use of these photographs and/or videos without compensation, on the BILGYM website or in any editorial, promotional or marketing material produced and/or published by BILGYM.</i></p>
                                </div>
                            </div>

                            <div class="row mt-4 agreement">
                                <div class="col-12 mb-3">
                                    <h6 class="mb-2 fancy-head"><span>VII. Pengabaian dan Pelepasan Tanggung Jawab / <i class="english">Waiver and Release of Liability</i></span></h6>
                                </div>

                                <div class="col-12">
                                    <ul style="list-style:none" class="pl-0 text-justify">
                                        <li class='mb-4'>
                                            <strong>(i) </strong>Ungkapan Asumsi Resiko: Saya, yang bertanda tangan di bawah ini secara sadar bahwa ada risiko yang signifikan dalam semua aspek pelatihan fisik. Risiko-risiko ini termasuk, tetapi tidak terbatas pada: jatuh, ketegangan otot, tarikan otot, robekan otot, patah tulang, cidera tulang kering, kelelahan karena udara panas, cedera pada lutut, cedera pada punggung, cedera pada kaki, serangan jantung, atau penyakit dan keluhan fisik lainnya yang mungkin saya alami yang dapat mengakibatkan cedera serius atau kematian; cedera atau kematian karena kelalaian yang saya lakukan sendiri, mitra pelatih saya, atau orang lain di sekitar saya; cedera atau kematian karena penggunaan peralatan yang tidak tepat atau kerusakan pada peralatan; keram dan terkilir. Saya menyadari bahwa semua risiko yang disebutkan di atas dapat mengakibatkan cedera serius atau kematian pada saya dan atau pasangan saya. Saya dengan sukarela betanggung jawab penuh atas risiko yang saya hadapi dan menerima tanggung jawab penuh atas cedera atau kematian yang mungkin timbul dari partisipasi dalam aktivitas atau kelas apa pun saat berada di, atau di bawah arahan BILGYM. Saya mengakui bahwa saya tidak memiliki gangguan fisik, cedera, atau penyakit yang akan membahayakan saya atau orang lain.<br><br>
                                            <i class="english"><strong>(i) </strong>Express Assumption of Risk: I, the undersigned, am aware that there are significant risks involved in all aspects of physical training. These risks include, but are not limited to: falls, muscle strains, muscle pulls, muscle tears, broken bones, shin splints, heat prostration, injuries to knees; , injuries to back, injuries to foot, heart attacks, or any other illness or soreness that I may incur which can result in serious injury or death; injury or death due to negligence on the part of myself, my training partner, or other people around me; injury or death due to improper use or failure of equipment; strains and sprains. I am aware that any of these above mentioned risks may result in serious injury or death to myself and or my partner(s). I willingly assume full responsibility for the risks that I am exposing myself to and accept full responsibility for any injury or death that may result from participation in any activity or class while at, or under direction of BILGYM. I acknowledge that I have no physical impairments, injuries , or illnesses that will endanger me or others. </i>
                                        </li>

                                        <li class='mb-4'>
                                            <strong>(ii)</strong> Pelepasan Tuntutan: Dengan mempertimbangkan risiko dan bahaya yang disebutkan di atas dan dengan mempertimbangkan fakta bahwa saya dengan sukarela berpartisipasi dalam kegiatan yang ditawarkan oleh BILGYM, saya, yang bertanda tangan di bawah ini dengan ini melepaskan BILGYM, direktur, pelatih, petugas, seluruh karyawan terkait serta agen dari setiap dan semua kewajiban, klaim, tuntutan, tindakan atau hak untuk menindak, yang terkait dengan, muncul dari, atau dengan cara apapun yang terkait dengan partisipasi saya dalam aktivitas ini, termasuk yang diduga disebabkan oleh tindakan lalai atau kelalaian pihak-pihak yang disebutkan di atas. Perjanjian ini akan mengikat saya, pasangan saya, anak-anak saya, wali saya, penerus saya, perwakilan, pewaris, pelaksana, penugasan, atau penerima transfer (secara kolektif disebut “Pelepas Tuntutan"). Saya menerima tanggung jawab secara penuh untuk keselamatan diri saya sendiri, peserta lainnya, para pengamat atau orang lain yang telah saya undang ke lokasi BILGYM, dan properti, dan saya menganggap risiko kerusakan, pencurian, kehilangan atau cedera adalah yang disebabkan oleh orang lain kepada saya, orang yang saya undang dan properti.<br>
                                            Jika ada bagian dari perjanjian ini yang dianggap tidak sah, saya setuju bahwa bagian lainnya dari perjanjian ini akan tetap memiliki kekuatan hukum yang penuh dan kuat. Jika saya menandatangani atas nama anak dibawah umur, saya juga memberikan izin penuh untuk setiap orang yang terhubung dengan BILGYM untuk mengelola pertolongan pertama yang dianggap perlu, dan dalam kasus penyakit atau cedera serius, saya memberikan izin untuk meminta perawatan medis dan atau bedah untuk anak dan untuk memindahkan anak ke fasilitas medis yang dianggap perlu untuk keselamatan dan kesehatan anak<br><br>
                                            <i class="english"><strong>(ii)</strong> Release: In consideration of the above mentioned risks and hazards and in consideration of the fact that I am willingly and voluntarily participating in the activities offered by BILGYM, I, the undersigned hereby release BILGYM, their directors, coaches, officers, employees, agents, from any and all liability, claims, demands, actions or rights of action, which are related to, arise out of, or are in any way connected with my participation in this activity, including those allegedly attributed to the negligent acts or omissions of the above mentioned parties . This agreement shall be binding upon me, my spouse, my children, my guardians, my successors, representatives, heirs, executors, assigns, or transferees (collectively, the "Releasors"). I accept full and complete responsibility for the safety of myself, any guests, observers or other individuals who I have invited to the venue of BILGYM, and property, and I assume the risk of damage, theft, loss or injury caused by others to me, my guests and property<br>
                                            If any portion of this agreement is held invalid, I agree that the remainder of the agreement shall remain in full legal force and effect. If I am signing on behalf of a minor child, I also give full permission for any person connected with BILGYM to administer first aid deemed necessary, and in case of serious illness or injury, I give permission to call for medical and or surgical care for the child and to transport the child to a medical facility deemed necessary for the wellbeing of the child.</i><br><br>
                                        </li>

                                        <li class='mb-4'>
                                            <strong>(iii)</strong> Ganti Rugi: Atas nama Pelepas Tuntutan, Peserta mengakui bahwa ada risiko yang terlibat dalam jenis kegiatan yang ditawarkan oleh BILGYM. Oleh karena itu, Peserta menerima tanggung jawab biaya atas cedera yang mungkin disebabkan oleh Peserta atau dirinya sendiri atau kepada peserta lain karena kelalaiannya. Jika pihak-pihak yang disebutkan di atas, atau siapa pun yang bertindak atas nama mereka, diharuskan untuk membayar biaya pengacara dan biaya untuk menegakan perjanjian ini, Peserta setuju untuk mengganti seluruh biaya tersebut. Peserta selanjutnya setuju untuk memberi ganti rugi dan melepaskan BILGYM, direktur, pelatih, petugas, karyawan dan agen mereka dari tanggung jawab atas cedera atau kematian seseorang dan kerusakan pada properti yang mungkin diakibatkan oleh kelalaian atau tindakan sengaja atau kelalaian Peserta saat berpartisipasi pada<br>
                                            ____ kegiatan yang ditawarkan oleh BILGYM, di bangunan utama atau di atas kendaraan dan lokasi apapun. Termasuk dan tidak terbatas pada taman, area rekreasi, taman bermain, area yang berdekatan dengan bangunan utama, dan / atau area apa pun yang dipilih untuk pelatihan oleh BILGYM.<br><br>
                                            
                                            Saya Telah membaca dan memahami asumsi risiko, dan pelepasan tuntutan dan saya memahami bahwa dengan menandatangani perjanjian ini akan mewajibkan saya untuk memberi ganti rugi kepada pihak-pihak yang disebutkan untuk pertanggungjawaban atas cedera atau kematian seseorang dan kerusakan properti yang disebabkan oleh tindakan yang disengaja atau kelalaian saya. Saya mengerti bahwa dengan menandatangani perjanjian pada formulir ini saya melepaskan hak hukum yang bernilai.<br><br>
                                            
                                            <i class="english"><strong>(iii)</strong> Indentification: On behalf of the Releasers, the Participant recognizes that there is risk involved in the types of activities offered by BILGYM. Therefore the Participant accepts financial responsibility for any injury that the Participant may cause either to him/herself or to any other participant due to his/her negligence. Should the above mentioned parties, or anyone acting on their behalf, be required to incur attorney's fees and costs to enforce this agreement, I agree to reimburse them for such fees and costs. I further agree to indemnify and hold harmless BILGYM, their directors, coaches, officers, employees, agents from liability for the injury or death of any person(s) and damage to property that may result from my negligent or intentional act or omission while participating in <br>
                                            _____ activities offered by BILGYM, at the main building or aboard. This includes but is not limited to parks, recreational areas, playgrounds, areas adjacent to main building, and/or any area selected for training by BILGYM. <br><br>

                                            I Have read and understood the foregoing assumption of risk, and release of liability and I understand that by signing it obligates me to indemnify the parties named for any liability for injury or death of any person and damage to property caused by my negligent or intentional act or omission. I understand that by signing this form I am waiving valuable legal rights.</i><br><br>
                                        </li>

                                        <li class='mb-4'>
                                            <strong>(iv)</strong> Berjanji Untuk Tidak Menggugat ke Pengadilan: Saya dengan ini setuju dan berjanji untuk tidak melakukannya, dan akan menyebabkan Pelepas Tuntutan tidak melakukan tindakan ini, termasuk tidak mengajukan tuntutan yang bertentangan, atau menuntut kompensasi dari atau yang melekat pada properti atau aset BILGYM atau siapapun termasuk para direktur, pelatih, petugas, karyawan, agen, untuk setiap kehilangan atau kerusakan yang timbul atau akibat dari keikutsertaan saya atau dalam perjalanan saya ke dan atau dari lokasi kegiatan yang diadakan oleh BILGYM, selanjutnya melepaskan dan menghentikan tuntutan kepada BILGYM atau para direktur, pelatih, petugas, karyawan dan agen mereka dari tanggung jawab atas klaim yang sedemikian rupa.<br><br>
                                            <i class="english"><strong>(iv)</strong> Promise Not To Bring Suit: I hereby agree and covenant not to, and shall cause the Releasors not to, bring a claim against, sue, demand compensation from or attach the property or assets of BILGYM or any of their directors, coaches, officers, employees, agents, for any loss or damage arising or resulting from my participation in BILGYM or my travel to or from or presence at the venue, 2nd forever release and discharge BILGYM or any of their directors, coaches, officers, employees, agents them from liability under such claims.</i>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row text-center mt-4 justify-content-center buttonbutton">
                        <div class="col-md-4">
                            <div class="text-center p-3 mb-3" style="border: 1px solid #e5e5e5;">
                                <div class="g-recaptcha" data-sitekey="6LcGiJoUAAAAAH7UI3Tf5FF93P6-ItuGSoMCPcLm" data-callback="enableBtn"></div>
                            </div>

                            <button class="btn btn-primary btn-lg btn-block btn-check" disabled>SUBMIT</button>

                            <button class="btn btn-primary btn-lg btn-block btn-submit agreement" disabled>SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5 after-registration">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body py-5 px-5">
                            <div class="text-medium text-center alert alert-success py-4 mb-5" style="font-size: 22px">
                                <p style="font-size: 16">Kami telah menerima data anda, silahkan datang ke gym kami dan beritahukan nama atau nomor telepon anda kepada petugas kami untuk melanjutkan rencana keanggotaan anda.</p>
                                <p style="font-size: 14"><i>We have received your data, please come to our club and tell your name or phone number to our front officer to continue with the membership plan.</i></p>
                            </div>

                            <div class="fancy-head"><span>Location</span></div>

                            <div id="maps" class="embed-responsive embed-responsive-21by9">

                            </div>

                            <div class="mt-4 text-right">
                                <a href="https://empirefitclub.com/" class="btn btn-primary btn-lg">Go to Website</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".agreement").hide();
            $(".after-registration").hide();


            var $ = jQuery;
            var api = "<?= $base_api ?>/health_questions";
            var branch_endpoint = "<?= $base_api ?>/branches";
            var area_endpoint = "<?= $base_api ?>/area";

            $.ajax({
                url: branch_endpoint,
                success: function(res) {
                    console.log(res['data']);
                    var branches = res['data'];
                    var addresses = [];
                    $.each(branches, function(i, branch) {
                        var branch_detail = `<div class="branch_wrapper" id="branch-` + branch.id + `">
                        <div>` + branch.address + `</div>
                        <div><strong>Phone:</strong>` + branch.phone + `</div>
                        <div><strong>Email:</strong>` + branch.email + `</div>
                    </div>`;
                        $('.addresses').append(branch_detail);

                        if (branch.branch_code == '0101') {
                            setTimeout(() => {
                                $("#maps").html(`<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.341862922066!2d106.79960821476917!3d-6.218570095498405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14d30079f01%3A0x2e74f2341fff266d!2sStadion+Utama+Gelora+Bung+Karno!5e0!3m2!1sid!2sid!4v1564556562030!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>`)
                            }, 200);
                        } else if (branch.branch_code == '0102') {
                            setTimeout(() => {
                                $("#maps").html(`<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.3419031479343!2d106.79960821460675!3d-6.218564762638148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14d30079f01%3A0x2e74f2341fff266d!2sStadion+Utama+Gelora+Bung+Karno!5e0!3m2!1sid!2sid!4v1562755324035!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>`)
                            }, 200);
                        } else {
                            setTimeout(() => {
                                $("#maps").html(`<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.341862922066!2d106.79960821476917!3d-6.218570095498405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14d30079f01%3A0x2e74f2341fff266d!2sStadion+Utama+Gelora+Bung+Karno!5e0!3m2!1sid!2sid!4v1564556562030!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>`)
                            }, 200);
                        }

                        var options = '<option value="' + branch.id + '">' + branch.branch_name + '</option>';
                        $('.branch-pick').append(options);

                        $('.branch-pick').on('change', function() {
                            var get_ID = $(this).val();
                            $('#branch-' + get_ID).show().siblings().hide();
                        });
                    });
                }
            });

            $.ajax({
                url: area_endpoint,
                success: function(res) {
                    var areas = res['data'];
                    $.each(areas, function(i, area) {
                        // console.log(area.id);
                        var looping_area = `<option value="` + area.id + `">` + area.area_name + `</option>`;
                        $('.area').append(looping_area);
                    });
                    setTimeout(() => {
                        $('.area').selectize();
                    }, 200);
                }
            });

            $.ajax({
                url: api,
                success: function(res) {
                    //console.log(data);
                    questions = res['data'];
                    var n = 0;
                    $.each(questions, function(i, item) {
                        // console.log(item);
                        if (item.id == 5) {
                            var _additional_question = '<div class="col-12"><div class="additional_question">How many times a week? <input type="text" name="how_many_times" class="form-control"/></div></div>';
                        } else if (item.id == 6) {
                            var _additional_question = '<div class="col-12"><div class="additional_question">if Yes what injuries or surgeries <input type="text" name="reason_in" class="form-control"/></div></div>';
                        } else {
                            var _additional_question = '';
                        }
                        //var markup = '<div class="list-question"><label for="question-'+item.id+'" class="d-block mb-4"><input type="checkbox" name="questions_'+item.id+'" id="question-'+item.id+'" value="'+item.id+'" class="mr-3"> '+item.question+'</label></div>';

                        var markup = '<div class="list-question"><div class="row align-items-center"><div class="col-md-9 mb-3 mb-md-0">' + item.question + '</div><div class="col-md-3"><select class="form-control" name="questions_' + item.id + '" id="question-' + item.id + '"><option value="1">Yes</option>Yes</option><option value="0">No</option></select></div>' + _additional_question + '</div></div>';

                        $('.questions').append(markup);

                    });
                }
            });

            $(document).on('click', '.btn-check', function() {
                // var idCard = $('input[name=idcard]').val();
                var branch = $('select[name=branch]').val();
                var name = $('select[name=firstName]').val();
                var email = $('input[name=email]').val();
                var contactNumber = $('input[name=contactnumber]').val();

                if (name == '') {
                    alert("Please insert your name");
                    return false;
                }

                if (branch == '') {
                    alert("Please select your Branch");
                    return false;
                }

                // if (idCard == '') {
                //     alert("Please insert your ID Card (KTP/SIM/)");
                //     return false;
                // }

                if (email == '') {
                    alert("Please insert your Email");
                    return false;
                }

                if (contactNumber == '') {
                    alert("Please insert your Contact Number");
                    return false;
                }

                var api = "<?= $base_api ?>/checkMember";
                $.ajax({
                    type: 'POST',
                    url: api,
                    data: {
                        // id_card: idCard,
                        email: email,
                        contactNumber: contactNumber
                    },
                    beforeSend: function() {
                        $('.btn-check').text('Loading...');
                        $('.btn-check').prop("disabled", true);
                    },
                    success: function(res) {
                        if (res.status == 200) {
                            setTimeout(function() {
                                $(".btn-check").hide();
                                var title = document.querySelector("h3:first-child");
                                //title.innerHTML ="Agreement";
                                $(".btn-check").hide();
                            }, 1000);
                            $(".agreement").show();
                        } else if (res.status == 422) {
                            setTimeout(function() {
                                var title = document.querySelector("h3:first-child");
                                title.innerHTML = "Thank You";
                                $(".select-branch").hide();
                                $(".before-registration").hide();
                                $(".after-registration").show();
                            }, 1000);
                            // window.location = "<?= $base_url ?>/submit.php";
                        } else {
                            console.log('something wrong!');
                        }
                    }
                });
            });

            $('.btn-submit').on('click', function() {
                // var idCard = $('input[name=idcard]').val();
                var branch = $('select[name=branch]').val();
                var firstName = $('input[name=firstName]').val();
                var nickName = $('input[name=nickName]').val();
                var email = $('input[name=email]').val();
                // var mobilePhone     = $('input[name=mobilephone]').val();
                // var address         = $('input[name=address]').val();
                var dateBirth = $('input[name=datebirth]').val();
                var contactNumber = $('input[name=contactnumber]').val();
                var emergency_name = $('input[name=emergency_contact_name]').val();
                var emergency = $('input[name=emergency_contact_number]').val();
                var note = $('textarea[name=reviewer_note]').val();
                var occupation = $('input[name=occupation]').val();
                // addtional
                var address = $('textarea[name=address]').val();
                var home_address_area = $('select[name=home_address_area]').val();
                var work_address = $('textarea[name=work_address]').val();
                var work_address_area = $('select[name=work_address_area]').val();

                answer = [];
                reason = [];
                $.each(questions, function(i, item) {

                    if (item.id == 5) {
                        answer.push($("#question-" + item.id).val());
                        reason.push($("input[name=how_many_times]").val());
                    } else if (item.id == 6) {
                        answer.push($("#question-" + item.id).val());
                        reason.push($("input[name=reason_in]").val());
                    } else {
                        answer.push($("#question-" + item.id).val());
                    }
                });
                var api = "<?= $base_api ?>/saveMember";

                $.ajax({
                    type: 'POST',
                    url: api,
                    data: {
                        name: firstName,
                        nickname: nickName,
                        email: email,
                        // phone           : mobilePhone,
                        // id_card_number: idCard,
                        date_of_birth: dateBirth,
                        phone: contactNumber,
                        emergency_name: emergency_name,
                        emergency: emergency,
                        note: note,
                        answer: answer,
                        reason: reason,
                        branch_id: branch,
                        occupation: occupation,
                        address: address,
                        home_address_area: home_address_area,
                        work_address: work_address,
                        work_address_area: work_address_area
                    },
                    beforeSend: function() {
                        $('.btn-submit').text('Loading...');
                        $('.btn-submit').prop("disabled", true);
                    },
                    success: function(res) {
                        if (res.status == 200) {
                            setTimeout(function() {
                                var title = document.querySelector("h3:first-child");
                                title.innerHTML = "Thank You";
                                $(".select-branch").hide();
                                $(".before-registration").hide();
                                $(".after-registration").show();
                            }, 1000);
                            // window.location = "<?= $base_url ?>/submit.php";
                        } else {
                            console.log('something wrong!');
                        }
                    }
                });
            });
        });

        function enableBtn() {
            $('.buttonbutton').find('.btn').removeAttr('disabled');
        }
    </script>
</body>

</html>