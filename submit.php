<?php
    session_start();
    $_SESSION = [];
    session_unset();
    session_destroy();
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Empire Fit | Online Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>
    
    <header class="py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-1 col-2">
                    <img src="images/logo.png" class="img-fluid" alt="">
                </div>
                <div class="col-md-11 col-10 text-right">
                    <h3>Thank you</h3>
                </div>
            </div>
        </div>
    </header>

    <section class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body py-5 px-5">
                            <div class="text-medium text-center alert alert-success py-4 mb-5" style="font-size: 22px">
                                <p style="font-size: 16">Kami telah menerima data anda, silahkan datang ke gym kami dan beritahukan nama atau nomor telepon anda kepada petugas kami untuk melanjutkan rencana keanggotaan anda.</p>
                                <p style="font-size: 14"><i>We have received your data, please come to our club and tell your name or phone number to our front officer to continue with the membership plan.</i></p>
                            </div>

                            <div class="fancy-head"><span>Location</span></div>

                            <div class="embed-responsive embed-responsive-21by9">
                                <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15865.47013236382!2d106.82032!3d-6.215166!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7c4cca17a3b48b14!2sEmpire+Fit+Club!5e0!3m2!1sen!2sid!4v1553514098734&key=AIzaSyCVYyvmQPt_mfiwuJLhxJFZRC7Vh2DOp-M" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>

                            <div class="mt-4 text-right">
                                <a href="https://empirefitclub.com/" class="btn btn-primary btn-lg">Go to Website</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</body>
</html>

